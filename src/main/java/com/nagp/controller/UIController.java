package com.nagp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagp.models.Employee;
import com.nagp.service.EmployeeService;

@RestController
@RequestMapping("employee")
public class UIController {

	@Autowired
	private EmployeeService service;

	@GetMapping(value = "/details")
	Employee[] fetchAllEmployees() {
		return service.getAllEmployee();
	}

	@PostMapping(value = "/new")
	void createEmployeeRecord(@RequestBody Employee employee) {
		service.saveEmployee(employee);
	}
}
