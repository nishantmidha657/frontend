package com.nagp.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nagp.models.Employee;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class EmployeeService {

	@Value("${nagp.backend.fetch.url}")
	private String backendFetchUrl;

	@Value("${nagp.backend.save.url}")
	private String backendSaveUrl;

	public RestTemplate restTemplate = new RestTemplate();

	public Employee[] getAllEmployee() {
		log.info("Calling url:{}", backendFetchUrl);
		return restTemplate.getForObject(backendFetchUrl, Employee[].class);
	}
	
	
	public void saveEmployee(Employee employee) {
		HttpHeaders requestHeaders = new HttpHeaders();
		Map<String, Object> paramMap = new HashMap<>();
	    requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	
		HttpEntity<?> postEntity = new HttpEntity<>(employee, requestHeaders);
		
		log.info("Calling url:{}", backendSaveUrl);
		ResponseEntity<String> responseEntity = restTemplate.exchange(
				backendSaveUrl, HttpMethod.POST, postEntity, String.class,
				paramMap);
		log.info("Response {}", responseEntity.getBody());
	}
}
