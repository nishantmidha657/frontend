package com.nagp.models;

import lombok.Data;

@Data
public class Employee {

	private String empId;
	private String empName;
	private String address;
}
